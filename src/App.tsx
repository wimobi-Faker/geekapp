import React, { Suspense } from 'react';
import './App.scss';
import { awaitImagesToLoad } from './startupServices';
import Image from './components/image/image';
import up from '/src/assets/images/UP.svg';

const Navbar = React.lazy(() => import('./components/navbar/navbar'));
const WelcomeSection = React.lazy(() => import('./sections/1-welcomeSection/welcomeSection'));
const WhoAreWeSection = React.lazy(() => import('./sections/2-whoAreWeSection/whoAreWeSection'));
const OurValuesSection = React.lazy(
	() => import('./sections/3-ourValuesSection/ourValuesSections'),
);
const OurTeamSection = React.lazy(() => import('./sections/4-ourTeamSection/ourTeamSection'));
const OurOfferSection = React.lazy(() => import('./sections/5-ourOfferSection/ourOfferSection'));
const OurSectorSection = React.lazy(() => import('./sections/6-ourSectorSection/ourSectorSection'));
const ContactUsSection = React.lazy(() => import('./sections/7-contactUsSection/contactUsSection'));
const Footer = React.lazy(() => import('./components/footer/footer'));

function App() {
	const [c, setC] = React.useState(0);
	const [d, setD] = React.useState(0);
	const inc = () => {
		setC((_) => _ + 1);
	};
	const closeDrawer = () => {
		setD((_) => _ + 1);
	};
	React.useEffect(() => {
		awaitImagesToLoad();
	}, []);
	return (
		<Suspense fallback={<div>Loading...</div>}>
			<Navbar closeDrawer={d} toWelcome={c} />
			<div onClick={closeDrawer} id='scrollContainer' className='scrollContainer'>
				<WelcomeSection />
				<WhoAreWeSection />
				<OurValuesSection />
				<OurTeamSection />
				<OurOfferSection />
				<OurSectorSection />
				<ContactUsSection />
				<Footer />
				<Image
					id={'scrollUpButton'}
					onClick={inc}
					draggable={false}
					className={`upArrow`}
					src={up}
					alt='up'
				/>
			</div>
		</Suspense>
	);
}

export default App;
