import logo from '/src/assets/images/logo.svg';

const stillLoading: Array<boolean> = [];
let alreadyPrinted = false;
let element: HTMLDivElement | null = null;

export const awaitImagesToLoad = () => {
	if (!alreadyPrinted) {
		addSplashScreen();
		alreadyPrinted = true;
	} else {
		stillLoading.splice(0);
	}
	if (document.readyState !== 'complete' || document.querySelectorAll('img').length === 0) {
		setTimeout(() => {
			awaitImagesToLoad();
		}, 200);
		return;
	}

	const imagesList = document.querySelectorAll('img');
	let index = 0;
	const onLoadOnErrorCb = () => {
		stillLoading.push(true);
		checkImages(imagesList.length);
	};
	while (index < imagesList.length) {
		if (imagesList[index].complete) {
			stillLoading.push(true);
		} else {
			imagesList[index].onload = onLoadOnErrorCb;
			imagesList[index].onerror = onLoadOnErrorCb;
		}
		index++;
	}
	checkImages(imagesList.length);
};

const checkImages = (length: number) => {
	if (length === stillLoading.length && stillLoading.every((_) => _)) {
		element?.classList.add('rootHidden');
		setTimeout(() => {
			document.body.removeChild(element as HTMLDivElement);
			document.getElementById('root')?.classList.add('rootShown');
			document.getElementById('root')?.classList.remove('rootHidden');
			// (document.querySelector(`a[href='${window.location.hash}']`) as HTMLAnchorElement)?.click();
			// console.log();
			// console.log(document.getElementById(''));
			const scrollCont = document.getElementById('scrollContainer');
			const upButton = document.getElementById('scrollUpButton');
			if (scrollCont && upButton) {
				scrollCont.addEventListener('scroll', () => {
					upButton.classList[scrollCont?.scrollTop >= 500 ? 'add' : 'remove'](`true`);
				});
			}
		}, 500);
	}
};

const addSplashScreen = () => {
	document.getElementById('root')?.classList.add('rootHidden');
	const img = document.createElement('img');
	img.src = logo;
	element = document.createElement('div');
	element.style.display = 'flex';
	element.style.alignItems = 'center';
	element.style.justifyContent = 'center';
	element.style.width = '100vw';
	element.style.height = '100vh';
	element.style.background = '#fff';
	element.style.position = 'fixed';
	element.style.top = '0';
	element.style.left = '0';
	element.style.zIndex = '999999999';
	element.appendChild(img);

	document.body.appendChild(element);
};
