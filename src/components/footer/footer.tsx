import React from 'react';
import './footer.scss';
import invertedLogo from '/src/assets/images/invertedLogo.svg';
// import footerIcon1 from '/src/assets/images/footerIcon1.svg';
// import footerIcon2 from '/src/assets/images/footerIcon2.svg';
// import footerIcon3 from '/src/assets/images/footerIcon3.svg';
import Image from '../image/image';

const Footer: React.FC = () => {
	return (
		<div className='footerMainContainer'>
			<Image draggable={false} className='invLogo' src={invertedLogo} alt='inverted_logo' />
			{/*<div className='contactList'>*/}
			{/*	<span className={'nosContact'}>Nos Contact</span>*/}
			{/*	<span>*/}
			{/*		<span className='x'>Adresse:</span> 52B rue Jules Ferry, 91390 Morsang sur orge*/}
			{/*	</span>*/}
			{/*	<span>*/}
			{/*		<span className='x'>Téléphone:</span> 0783160619*/}
			{/*	</span>*/}
			{/*	<span>*/}
			{/*		<span className='x'>Mail:</span> mghebaji@geek-app.net*/}
			{/*	</span>*/}
			{/*	<span>*/}
			{/*		<span className='x'>Linkedin:</span>{' '}*/}
			{/*		<a target='_blank' href='https://www.linkedin.com/company/geekapp/' rel='noreferrer'>*/}
			{/*			https://www.linkedin.com/company/geekapp/*/}
			{/*		</a>*/}
			{/*	</span>*/}
			{/*</div>*/}
			<span>©GEEKAPP2023</span>
			{/*<div className='contactContainer'>*/}
			{/*	*/}
			{/*	/!*<div className='logos'>*!/*/}
			{/*	/!*	<span>Rejoignez-nous</span>*!/*/}
			{/*	/!*	<Image draggable={false} src={footerIcon1} alt='Linkedin' />*!/*/}
			{/*	/!*	<Image draggable={false} src={footerIcon2} alt='Instagram' />*!/*/}
			{/*	/!*	<Image draggable={false} src={footerIcon3} alt='Facebook' />*!/*/}
			{/*	/!*</div>*!/*/}
			{/*</div>*/}
		</div>
	);
};

export default Footer;
