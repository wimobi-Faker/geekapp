import React, { useRef } from 'react';

interface PropsInterface extends React.ComponentPropsWithoutRef<'img'> {
	name?: () => void;
}

const Image: React.FC<PropsInterface> = (props) => {
	const imgRef = useRef<HTMLImageElement>(null);

	return <img ref={imgRef} alt='' {...props} />;
};

export default Image;
