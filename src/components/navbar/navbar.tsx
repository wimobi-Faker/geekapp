import React, { useEffect, useRef } from 'react';
import './navbar.scss';
import logo from '/src/assets/images/logo.svg';
import burger from '/src/assets/images/burger.svg';
import Image from '../image/image';

const Navbar: React.FC<{ toWelcome: number; closeDrawer: number }> = ({
	toWelcome,
	closeDrawer,
}) => {
	const [active, setActive] = React.useState(0);
	const [isOpen, setIsOpen] = React.useState(false);
	const indicatorRef = React.useRef<HTMLDivElement>(null);

	useEffect(() => {
		const link = document.querySelector('.link.true') as HTMLElement | null;
		if (indicatorRef.current && !!link) {
			if (window.innerWidth <= 768) {
				indicatorRef.current.style.top = `${link.offsetTop}px`;
			} else {
				indicatorRef.current.style.left = `${link.offsetLeft - link.clientHeight}px`;
			}
		}
	}, [active]);

	const getLinks = () => (
		<div className={`linksContainer ${isOpen}`}>
			<a href='#' onClick={goTo1} className={`link ${active === 0}`}>
				Acceuil
			</a>
			<a href={'#whoAreWe'} className={`link ${active === 1}`}>
				Qui somme nous ?
			</a>
			<a href={'#ourValues'} className={`link ${active === 2}`}>
				Nos valeurs
			</a>
			<a href={'#ourTeam'} className={`link ${active === 3}`}>
				Notre équipe
			</a>
			<a href={'#ourOffer'} className={`link ${active === 4}`}>
				Nos services
			</a>
			<a href={'#ourSector'} className={`link ${active === 5}`}>
				Nos Clients & Partenaires
			</a>
			<a href={'#contactUs'} className={`link ${active === 6}`}>
				Contact
			</a>
			<div ref={indicatorRef} className={'indicator'} />
		</div>
	);

	const goTo1 = () => {
		document.getElementById('scrollContainer')?.scrollTo({
			top: 0,
			left: 0,
			behavior: 'smooth',
		});
	};

	useEffect(() => {
		goTo1();
	}, [toWelcome]);

	useEffect(() => {
		setIsOpen(false);
	}, [closeDrawer]);

	const observersArray = useRef<Array<{ observer: IntersectionObserver; _: Element }>>([]);
	const intersections = useRef<Array<{ index: number; value: number }>>([]);
	useEffect(() => {
		const buildThresholdList = () => {
			const thresholds = [];
			const numSteps = 20;

			for (let i = 1.0; i <= numSteps; i++) {
				const ratio = i / numSteps;
				thresholds.push(ratio);
			}

			thresholds.push(0);
			return thresholds;
		};

		const linksReference: Array<Element> = [
			document.getElementById('welcome') as Element,
			document.getElementById('whoAreWe') as Element,
			document.getElementById('ourValues') as Element,
			document.getElementById('ourTeam') as Element,
			document.getElementById('ourOffer') as Element,
			document.getElementById('ourSector') as Element,
			document.getElementById('contactUs') as Element,
		];
		const callback = (
			entries: IntersectionObserverEntry[],
			observer: IntersectionObserver,
			index: number,
		) => {
			entries.forEach((_) => {
				const ind = intersections.current.findIndex((__) => __.index === index);
				if (ind >= 0) {
					intersections.current[ind].value = _.intersectionRatio;
				} else {
					intersections.current.push({ index, value: _.intersectionRatio });
				}
			});
			setActive(
				intersections.current.reduce((prev, curr) => {
					return prev.value > curr.value ? prev : curr;
				}).index,
			);
		};

		linksReference.forEach((_, _index) => {
			const observer = new IntersectionObserver(
				(entries) => {
					callback(entries, observer, _index);
				},
				{
					root: document.getElementById('scrollContainer'),
					rootMargin: '0px',
					threshold: buildThresholdList(),
				},
			);
			observer.observe(_);
			observersArray.current.push({ observer, _ });
		});

		return () => {
			observersArray.current.forEach((_) => {
				_.observer.unobserve(_._);
			});
			observersArray.current.splice(0);
		};
	}, []);

	const open = () => {
		setIsOpen((_) => !_);
	};

	return (
		<div className='navbarMainContainer shadowed'>
			<Image draggable={false} src={logo} alt='logo_' />
			<Image onClick={open} className='burgerIcon' src={burger} alt='burger_tile' />
			{getLinks()}
		</div>
	);
};

export default Navbar;
