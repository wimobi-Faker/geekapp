import React, { LegacyRef, useEffect } from 'react';
import './input.scss';

const Input: React.FC<{
	clear: boolean;
	placeholder: string;
	textArea?: boolean;
	returnedValue: (value: string) => void;
}> = ({ clear, placeholder, textArea, returnedValue }) => {
	const inputRef = React.useRef<HTMLInputElement | HTMLTextAreaElement>(null);
	const labelRef = React.useRef<HTMLSpanElement>(null);

	const onChange = (value: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
		returnedValue(value.target.value.trim());
	};

	React.useEffect(() => {
		const onFocus = () => {
			labelRef.current?.classList.add('focused');
		};
		const onBlur = () => {
			if (inputRef.current?.value.trim().length === 0) {
				labelRef.current?.classList.remove('focused');
			}
		};
		if (inputRef.current && labelRef.current) {
			onFocus();
			onBlur();
			labelRef.current?.classList.add(`${textArea}`);
			inputRef.current.addEventListener('focus', onFocus);
			inputRef.current.addEventListener('blur', onBlur);
		}

		return () => {
			if (inputRef.current && labelRef.current) {
				inputRef.current.removeEventListener('focus', onFocus);
				inputRef.current.removeEventListener('blur', onBlur);
			}
		};
	}, []);

	useEffect(() => {
		if (clear && inputRef.current) {
			inputRef.current.value = '';
			labelRef.current?.classList.remove('focused');
		}
	}, [clear]);

	return (
		<div className='inputContainer'>
			<span ref={labelRef} className='label'>
				{placeholder}
			</span>
			{textArea ? (
				<textarea
					rows={2}
					defaultValue={''}
					ref={inputRef as LegacyRef<HTMLTextAreaElement> | undefined}
					onChange={onChange}
				/>
			) : (
				<input
					defaultValue={''}
					ref={inputRef as LegacyRef<HTMLInputElement> | undefined}
					type='text'
					onChange={onChange}
				/>
			)}
		</div>
	);
};

export default Input;
