import React from 'react';
import './whoAreWeSection.scss';
import whoAreWeImage from '/src/assets/images/whoAreWeSectionImage.png';
import Image from '../../components/image/image';

const WhoAreWeSection: React.FC = () => {
	return (
		<div id='whoAreWe' className='whoAreWeSectionMainContainer'>
			<span className='titleWithShapes'>Qui somme nous ?</span>
			<div className='CCOONNTT'>
				<div className='leftSideContainer'>
					{/*<span className='titleWithShapes'>Qui somme nous ?</span>*/}
					<div className='details'>
						<span>
							Votre partenaire de confiance <br /> dans vos ambitions digitales.
						</span>
						<span>
							Créé en 2018, GEEKAPP met au service de ses clients de tout bord un panel complet de
							services de conseil en stratégie et en transformation digitale. Elle se distingue par
							son savoir-faire qui aide les entreprises et les institutions à tirer le meilleur du
							digital flow.
						</span>
					</div>
				</div>
				<div className='rightSideContainer'>
					<Image draggable={false} src={whoAreWeImage} alt='who_are_we_image' />
				</div>
			</div>
		</div>
	);
};

export default WhoAreWeSection;
