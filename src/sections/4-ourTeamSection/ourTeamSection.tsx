import React, { useEffect, useRef } from 'react';
import './ourTeamSection.scss';
import ourTeamSectionImage from '/src/assets/images/ourTeamSectionImage.jpg';
// import ourTeamIcon1 from '/src/assets/images/ourTeamIcon1.svg';
import ourTeamIcon2 from '/src/assets/images/ourTeamIcon2.svg';
import ourTeamIcon3 from '/src/assets/images/ourTeamIcon3.svg';
import ourTeamIcon4 from '/src/assets/images/ourTeamIcon4.svg';
import Image from '../../components/image/image';

const OurTeamSection: React.FC = () => {
	const employeesRef = React.useRef<HTMLSpanElement>(null);
	const experienceRef = React.useRef<HTMLSpanElement>(null);
	const clientsRef = React.useRef<HTMLSpanElement>(null);
	const alreadyObserved = useRef(false);

	useEffect(() => {
		if (employeesRef.current && !alreadyObserved.current) {
			alreadyObserved.current = true;
			const observer = new IntersectionObserver(
				(entries) => {
					if (entries[0]?.isIntersecting) {
						restartCounter(employeesRef);
						restartCounter(experienceRef);
						restartCounter(clientsRef);
					}
				},
				{
					root: document,
					rootMargin: '0px',
					threshold: 1,
				},
			);
			observer.observe(employeesRef.current);
		}
	}, []);

	const restartCounter = (whichOne: React.RefObject<HTMLSpanElement>) => {
		if (whichOne.current) {
			const hasPlus = whichOne.current.innerText.indexOf('+ ');
			const dataCount = parseInt(whichOne.current.dataset.count as string);
			const printedValue = parseInt(
				whichOne.current?.innerText?.slice(hasPlus >= 0 ? hasPlus + 2 : 0),
			);
			const factor = 250 - (printedValue / dataCount) * 200;
			if (dataCount <= printedValue) {
				return;
			}
			whichOne.current.innerText = `${hasPlus > -1 ? '+ ' : ''}${printedValue + 1}`;
			setTimeout(() => {
				restartCounter(whichOne);
			}, factor);
		}
	};
	return (
		<div id='ourTeam' className='ourTeamSectionMainContainer'>
			<span className='titleWithShapes'>Notre équipe</span>
			<div className='cont'>
				<div className='left'>
					<Image draggable={false} alt='our_team_image' src={ourTeamSectionImage} />
				</div>
				<div className='right'>
					<span className='text'>
						Grâce à sa culture multi-compétences, GEEKAPP est en mesure de répondre à vos besoins
						spécifiques dans tout le cycle de vie de votre projet IT en mettant à votre disposition
						des consultants compétents et certifiés capables de concevoir, réaliser, tester et
						déployer des solutions web et mobiles innovantes.
					</span>
					<div className='details'>
						<div className='item'>
							<span className='icon'>
								<Image
									style={{
										width: '3.177083333vw',
										aspectRatio: '1.12962963',
									}}
									draggable={false}
									src={ourTeamIcon2}
									alt={'icon_'}
								/>
							</span>
							<span className='value' ref={employeesRef} data-count={'12'}>
								+ 0
							</span>
							<span className='label'>Collaborateurs</span>
						</div>
						<div className='item'>
							<span className='icon'>
								<Image
									style={{
										width: '2.8vw',
										aspectRatio: '1.016666667',
									}}
									draggable={false}
									src={ourTeamIcon3}
									alt={'icon_'}
								/>
							</span>
							<span className='value' ref={experienceRef} data-count={'4'}>
								+ 0
							</span>
							<span className='label'>Ans d’expériences</span>
						</div>
						<div className='item'>
							<span className='icon'>
								<Image
									style={{
										width: '2.8vw',
										aspectRatio: '1.017857143',
									}}
									draggable={false}
									src={ourTeamIcon4}
									alt={'icon_'}
								/>
							</span>
							<span className='value' ref={clientsRef} data-count={'20'}>
								0
							</span>
							<span className='label'>Partenaires et Clients</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default OurTeamSection;
