import React, { useEffect, useState } from 'react';
import './contactUsSection.scss';
import contactUsImage from '/src/assets/images/contactUsSectionImage.jpg';
import Input from '../../components/input/input';
import Image from '../../components/image/image';

const ContactUsSection: React.FC = () => {
	const [hintMessage, setHintMessage] = useState('');
	const formValues = React.useRef<Record<string, string>>({
		name: '',
		email: '',
		message: '',
	});
	const returnedValue = (key: string, value: string) => {
		formValues.current[key] = value;
	};

	const submitForm = () => {
		if (
			[
				formValues.current['name'],
				formValues.current['email'],
				formValues.current['message'],
			].includes('')
		) {
			setHintMessage('Tous les champs sont requis');
		} else if (!/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(formValues.current['email'])) {
			setHintMessage("L'email n'est pas valide.");
		} else {
			fetch('https://api-wimobi.000webhostapp.com/geekapp/contact', {
				method: 'post',
				body: JSON.stringify({
					name: formValues.current['name'],
					email: formValues.current['email'],
					message: formValues.current['message'],
				}),
			})
				.then((res) => res.json())
				.then((res) => {
					setHintMessage('Email envoyé');
					formValues.current = {
						name: '',
						email: '',
						message: '',
					};
				});
		}
	};

	useEffect(() => {
		if (hintMessage.length > 0) {
			setTimeout(() => {
				setHintMessage('');
			}, 3000);
		}
	}, [hintMessage]);

	const openUrl = (which: number) => {
		const aEl = document.createElement('a');
		if (which === 0) {
			aEl.href = 'https://www.linkedin.com/company/geekapp/';
			aEl.target = '_blank';
		} else {
			aEl.href = `mailto:mghebaji@geek-app.net`;
		}
		aEl.click();
	};

	return (
		<div id='contactUs' className='contactUsSectionMainContainer'>
			<div className={`hintMessage ${hintMessage.length > 0}`}>{hintMessage}</div>
			<span className='titleWithShapes'>Contactez-nous</span>
			<div className='container'>
				<div className='left'>
					<Image draggable={false} src={contactUsImage} alt='contacter_nous_image' />
				</div>
				<div className='right'>
					<Input
						clear={hintMessage === 'Email envoyé'}
						returnedValue={(value: string) => returnedValue('name', value)}
						placeholder='Nom'
					/>
					<Input
						clear={hintMessage === 'Email envoyé'}
						returnedValue={(value: string) => returnedValue('email', value)}
						placeholder='Email'
					/>
					<Input
						clear={hintMessage === 'Email envoyé'}
						returnedValue={(value: string) => returnedValue('message', value)}
						placeholder='Message'
						textArea
					/>
					<button onClick={submitForm}>Envoyer</button>
				</div>
			</div>
			<div className='contactListCont'>
				<div className={'contact'}>
					<span className={'title'}>Adresse</span>
					<span>52B rue Jules Ferry, 91390 Morsang sur orge</span>
				</div>
				<div className={'contact'}>
					<span className={'title'}>Téléphone</span>
					<span>0783160619</span>
				</div>
				<div style={{ cursor: 'pointer' }} className={'contact'} onClick={() => openUrl(1)}>
					<span className={'title mail'}>Mail</span>
					<span>mghebaji@geek-app.net</span>
				</div>
				<div style={{ cursor: 'pointer' }} className={'contact'} onClick={() => openUrl(0)}>
					<span className={'title'}>Linkedin</span>
					<span>Geekapp</span>
				</div>
			</div>
		</div>
	);
};

export default ContactUsSection;
