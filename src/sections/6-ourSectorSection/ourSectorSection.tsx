import React from 'react';
import './ourSectorSection.scss';
import { Swiper, SwiperSlide } from 'swiper/react';
// import ourSectorIcon1 from '/src/assets/images/ourSectorIcon1.svg';
// import ourSectorIcon2 from '/src/assets/images/ourSectorIcon2.svg';
// import ourSectorIcon3 from '/src/assets/images/ourSectorIcon3.svg';
// import ourSectorIcon4 from '/src/assets/images/ourSectorIcon4.svg';
import clientIcon1 from '/src/assets/images/clientIcon1.svg';
import clientIcon2 from '/src/assets/images/clientIcon2.svg';
import clientIcon3 from '/src/assets/images/clientIcon3.svg';
import clientIcon4 from '/src/assets/images/clientIcon4.svg';
import clientIcon5 from '/src/assets/images/clientIcon5.svg';
import clientIcon6 from '/src/assets/images/clientIcon6.svg';
import clientIcon7 from '/src/assets/images/clientIcon7.svg';
import clientIcon8 from '/src/assets/images/clientIcon8.svg';
import clientIcon9 from '/src/assets/images/clientIcon9.svg';
import Image from '../../components/image/image';
import 'swiper/css';
import { Autoplay, FreeMode } from 'swiper';

const OurSectorSection: React.FC = () => {
	return (
		<div id='ourSector' className='ourSectorSectionMainContainer'>
			<span className='titleWithShapes'>Nos Clients & Partenaires</span>
			<span className='text1'>Ils nous font confiance</span>
			<Swiper
				spaceBetween={50}
				autoplay={{
					delay: 1000,
					disableOnInteraction: false,
				}}
				modules={[Autoplay, FreeMode]}
				slidesPerView='auto'
				loop={true}
				freeMode={true}
			>
				<SwiperSlide>
					<Image draggable={false} src={clientIcon1} alt='seteur_1' />
				</SwiperSlide>
				<SwiperSlide>
					<Image draggable={false} src={clientIcon2} alt='seteur_2' />
				</SwiperSlide>
				<SwiperSlide>
					<Image draggable={false} src={clientIcon3} alt='seteur_3' />
				</SwiperSlide>
				<SwiperSlide>
					<Image draggable={false} src={clientIcon4} alt='seteur_4' />
				</SwiperSlide>
				<SwiperSlide>
					<Image draggable={false} src={clientIcon5} alt='seteur_5' />
				</SwiperSlide>
				<SwiperSlide>
					<Image draggable={false} src={clientIcon6} alt='seteur_6' />
				</SwiperSlide>
				<SwiperSlide>
					<Image draggable={false} src={clientIcon7} alt='seteur_7' />
				</SwiperSlide>
				<SwiperSlide>
					<Image draggable={false} src={clientIcon8} alt='seteur_8' />
				</SwiperSlide>
				<SwiperSlide>
					<Image draggable={false} src={clientIcon9} alt='seteur_9' />
				</SwiperSlide>
			</Swiper>

			{/*<span className='text2'>*/}
			{/*	Geekapp se développe et se diversifie dans d’autres secteurs tels que le Voyage et tourisme,*/}
			{/*	le Retail & E-Commerce et le Luxe.*/}
			{/*</span>*/}
			{/*<div className='cardsContainer'>*/}
			{/*	<div className='card'>*/}
			{/*		<Image draggable={false} src={ourSectorIcon1} alt='seteur_' />*/}
			{/*		<span>Voyage & Tourisme</span>*/}
			{/*	</div>*/}
			{/*	<div className='card'>*/}
			{/*		<Image draggable={false} src={ourSectorIcon2} alt='seteur_' />*/}
			{/*		<span>Retail & E-commerce</span>*/}
			{/*	</div>*/}
			{/*	<div className='card'>*/}
			{/*		<Image draggable={false} src={ourSectorIcon3} alt='seteur_' />*/}
			{/*		<span>Medias & Divertissement</span>*/}
			{/*	</div>*/}
			{/*	<div className='card'>*/}
			{/*		<Image draggable={false} src={ourSectorIcon4} alt='seteur_' />*/}
			{/*		<span>Banque & Assurance</span>*/}
			{/*	</div>*/}
			{/*</div>*/}
		</div>
	);
};

export default OurSectorSection;
