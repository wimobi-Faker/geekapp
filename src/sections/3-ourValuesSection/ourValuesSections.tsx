import React from 'react';
import './ourValuesSection.scss';
import ourValuesCard1Icon from '/src/assets/images/ourValuesCard1Icon.svg';
import ourValuesCard2Icon from '/src/assets/images/ourValuesCard2Icon.svg';
import ourValuesCard3Icon from '/src/assets/images/ourValuesCard3Icon.svg';
import Image from '../../components/image/image';

const OurValuesSection: React.FC = () => {
	return (
		<div id='ourValues' className='ourValuesSectionMainContainer'>
			<span className='titleWithShapes'>Nos valeurs</span>
			<span className='description'>
				GEEKAPP est un véritable incubateur de ressources compétentes qui se mettent à la tâche
				chaque jour pour apporter à leurs clients une grande valeur ajoutée peu importe les défis
				rencontrés.
			</span>
			<div className='cardsContainer'>
				<div className='card'>
					<Image draggable={false} src={ourValuesCard1Icon} alt='icon_' />
					<span className='title'>EXPERTISE ET PERFORMANCE</span>
					<span className='text'>
						Nous proposons à nos clients une offre IT globale et digitale de bout en bout. Une offre
						pertinente et efficace sur toute la chaîne de valeurs des entreprises grâce à une
						parfaite maîtrise des technologies.
					</span>
				</div>
				<div className='card'>
					<Image draggable={false} src={ourValuesCard2Icon} alt='icon_' />
					<span className='title'>EVOLUTION PERMANAENTE</span>
					<span className='text'>
						GEEKAPP aide ses collaborateurs à s’épanouir et à développer pleinement leur potentiel
						et leurs compétences techniques en contribuant à des projets majeurs et à très fort
						impact sur la société et sur l’économie.
					</span>
				</div>
				<div className='card'>
					<Image draggable={false} src={ourValuesCard3Icon} alt='icon_' />
					<span className='title'>La responsabilité</span>
					<span className='text'>
						Rendre les clients heureux est le noyau dur de nos services . Des communications
						régulières, des mesures proactives et un dévouement général au besoin de notre client,
						sont nos atouts pour livrer leurs ambitions numériques.
					</span>
				</div>
			</div>
		</div>
	);
};

export default OurValuesSection;
