import React from 'react';
import './ourOfferSection.scss';
import ourOfferIcon1 from '/src/assets/images/ourOfferIcon1.svg';
import ourOfferIcon2 from '/src/assets/images/ourOfferIcon2.svg';
import Image from '../../components/image/image';

const OurOfferSection: React.FC = () => {
	return (
		<div id='ourOffer' className='ourOfferSectionMainContainer'>
			<span className='titleWithShapes'>Nos services</span>
			<div className='cardsContainer'>
				<div className='weOffer'>
					<span className='shaped'>Nous offrons</span>
					<span>les meilleurs services</span>
				</div>
				<div className='cardItem'>
					<span className='icon'>
						<Image draggable={false} src={ourOfferIcon1} alt='icon_' />
					</span>
					<span className='title'>Consulting WEB</span>
					<span className='text'>
						Une équipe de consultants WEB qualifiée est mise à votre disposition et vous assistera
						de l’étude jusqu’à la livraison pour aboutir votre projet au succès et augmenter votre
						productivité et votre efficacité.
					</span>
					<span className={'techStackTitle'}>Frontend:</span>
					<div className={'techStack'}>
						<span>Angular</span>
						<span>ReactJs</span>
						<span>VueJs</span>
						<span>HTML</span>
						<span>CSS</span>
					</div>
					<span className={'techStackTitle'}>Backend:</span>
					<div className={'techStack'}>
						<span>PHP</span>
						<span>Laravel</span>
						<span>Symfony</span>
						<span>NodeJS</span>
						<span>.NET</span>
						<span>JAVA</span>
						<span>JEE</span>
					</div>
				</div>
				<div className='cardItem'>
					<span className='icon'>
						<Image draggable={false} src={ourOfferIcon2} alt='icon_' />
					</span>
					<span className='title'>Consulting MOBILE</span>
					<span className='text'>
						Fort de son savoir-faire, GEEKAPP accompagne l’ensemble de ses clients vers la réussite
						et l’aboutissement de leurs projets mobiles gravitant autour une panoplie de
						technologies et de cross-plateformes les plus avancés et les plus adaptés sur le marché.
					</span>
					<span className={'techStackTitle'}>Les technologies</span>
					<div className={'techStack'}>
						<span>Android</span>
						<span>IOS</span>
						<span>React Native</span>
						<span>Flutter</span>
					</div>
				</div>
			</div>
		</div>
	);
};

export default OurOfferSection;
