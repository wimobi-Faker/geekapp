import React from 'react';
import './welcomeSection.scss';
import welcomeImage from '/src/assets/images/welcomeSectionImage.jpg';
import Image from '../../components/image/image';

const WelcomeSection: React.FC = () => {
	return (
		<div id='welcome' className='welcomeSectionMainContainer'>
			<div className={'descriptionContainer'}>
				<div className='title'>
					<span>Profitez</span>
					<span>de notre expertise humaine pour une transformation digitale réussie !</span>
				</div>
				<div className='text'>
					<span>
						GEEKAPP est une agence de consulting IT et de services numériques qui vous propose une
						expertise humaine de bout en bout.
					</span>
				</div>
			</div>
			<Image
				draggable={false}
				alt={'welcome_image'}
				src={welcomeImage}
				className={'backgroundImage'}
			/>
		</div>
	);
};

export default WelcomeSection;
